.PHONY: test
test:
	poetry run pytest --cov=fun_coverage --cov-report=term-missing tests/fun_coverage_tests -vv
	poetry run fun-coverage --cov-fail-under=100
	rm .coverage
