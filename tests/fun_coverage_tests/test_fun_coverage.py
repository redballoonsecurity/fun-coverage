from dataclasses import dataclass
from typing import List, Dict

import os
import pytest
import re
import sys

from fun_coverage.fun_coverage import main, script_entry_point

EXAMPLE_TEST_SUITE_PREFIX = "tests/example_test_suite/src/"


@dataclass
class FileReportInfo:
    """Parsed information from a line of the text report of fun-coverage."""

    funcs: int
    miss: int
    cover: str
    missing: List[int]


def full_report_filename_from_base(base_filename: str) -> str:
    return EXAMPLE_TEST_SUITE_PREFIX + base_filename


def get_file_reports_from_full_report(report_lines: List[str]) -> List[str]:
    """
    Return the lines between the two horizontal rules ("-------"...), corresponding to the individual
    file reports, from the full fun-coverage report.
    """
    boundaries = []
    for index, line in enumerate(report_lines):
        if line.startswith("-----"):
            boundaries.append(index)
    return report_lines[boundaries[0] + 1 : boundaries[1]]


def parse_file_report_line(line: str) -> Dict[str, FileReportInfo]:
    """
    Return a single-key dictionary {filename: report_info} parsed from a line of output of the fun-coverage report.
    """
    file_report_regex = (
        r"(?P<filename>[^\s]+)\s+"
        r"(?P<funcs>\d+)\s+"
        r"(?P<miss>\d+)\s+"
        r"(?P<cover>\d+%)\s+"
        r"(?P<missing>[\d, ]*)"
    )
    match = re.match(file_report_regex, line)
    assert match is not None, line
    filename = match.group("filename")
    funcs = int(match.group("funcs"))
    miss = int(match.group("miss"))
    cover = match.group("cover")
    missing_str = match.group("missing")
    missing = [int(number) for number in missing_str.split(", ") if number != ""]
    return {filename: FileReportInfo(funcs, miss, cover, missing)}


@pytest.fixture(scope="session")
def run_example_test_suite(pytestconfig):
    """Run the provided example test suite, asking for the line coverage of its `src` subdirectory."""
    rootdir = pytestconfig.rootdir
    example_test_suite_path = rootdir / "tests" / "example_test_suite"
    os.system(f"poetry run pytest --cov={example_test_suite_path}/src {example_test_suite_path}")
    yield
    os.remove(".coverage")


@pytest.fixture
def fun_coverage_output(run_example_test_suite, capsys) -> str:
    """Return the stdout of fun-coverage run with default arguments."""
    main(".coverage", None)
    out, _ = capsys.readouterr()
    return out


@pytest.fixture
def parsed_fun_coverage_output(fun_coverage_output) -> Dict[str, FileReportInfo]:
    lines = fun_coverage_output.splitlines()
    lines_to_parse = get_file_reports_from_full_report(lines)
    file_reports = {}
    for line in lines_to_parse:
        file_reports.update(parse_file_report_line(line))
    return file_reports


def test_docstring_only_function(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[
        full_report_filename_from_base("docstring_only_function.py")
    ]
    assert report_info == FileReportInfo(1, 0, "100%", [])


def test_non_covered_function(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[
        full_report_filename_from_base("non_covered_function.py")
    ]
    assert report_info == FileReportInfo(5, 4, "20%", [1, 6, 14, 24])


def test_overload(parsed_fun_coverage_output):
    """
    Test that overloaded functions are excluded from function coverage reporting.
    """
    report_info = parsed_fun_coverage_output[
        full_report_filename_from_base("overload.py")
    ]
    assert report_info == FileReportInfo(1, 0, "100%", [])


def test_covered_function_with_partially_covered_lines(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[
        full_report_filename_from_base("covered_function_with_partially_covered_lines.py")
    ]
    assert all((report_info.funcs == 1, report_info.miss == 0, report_info.cover == "100%"))


def test_async_function(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[full_report_filename_from_base("async_function.py")]
    assert report_info == FileReportInfo(1, 0, "100%", [])


def test_excluded_function(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[full_report_filename_from_base("excluded_function.py")]
    assert all((report_info.funcs == 0, report_info.miss == 0, report_info.cover == "100%"))


def test_methods(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[full_report_filename_from_base("methods.py")]
    if sys.version_info.minor <= 7:
        function_lines = [9, 12, 15]
    else:
        function_lines = [9, 12, 16]
    # The abstractmethod should not be listed as a function
    assert report_info == FileReportInfo(3, 3, "0%", function_lines)


def test_single_statement_funcs(parsed_fun_coverage_output):
    report_info = parsed_fun_coverage_output[full_report_filename_from_base("multiline_statements.py")]
    assert all((report_info.funcs == 3, report_info.miss == 0, report_info.cover == "100%"))


def test_unsatisfied_min_coverage(run_example_test_suite, capsys):
    with pytest.raises(SystemExit):
        main(".coverage", 100)
    out, _ = capsys.readouterr()
    assert "% not reached" in out


def test_satisfied_min_coverage(run_example_test_suite, capsys):
    main(".coverage", 0)
    out, _ = capsys.readouterr()
    assert "% reached" in out


def test_script_entry_point():
    sys.argv = ["fun-coverage"]
    script_entry_point()
