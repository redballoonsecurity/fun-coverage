import pytest

from .src.async_function import async_function
from .src.covered_function_with_partially_covered_lines import (
    covered_function_with_partially_covered_lines,
)
from .src.docstring_only_function import docstring_only_function
from .src import multiline_statements



def test_docstring_only_function():
    docstring_only_function()


def test_covered_function_with_partially_covered_lines():
    covered_function_with_partially_covered_lines(0)


def test_single_multiline_statement():
    multiline_statements.normal_multiline_statement()
    multiline_statements.odd_format_multiline_statement()
    multiline_statements.starts_with_assignment()


def test_non_covered_functions():
    # Import non_covered_functions to ensure that function statement lines are marked as covered
    from .src import non_covered_function


def test_overload():
    from .src.overload import return_something
    assert return_something(None) == "None"
    assert return_something(b"hello") == b"hello"


def test_methods():
    # Import file to ensure that function statement lines are marked as covered
    from .src.methods import ExampleClass

@pytest.mark.asyncio
async def test_async_function():
    await async_function()
