def excluded_function():  # pragma: no cover
    """
    This function is excluded from coverage.py, so it's also excluded from fun-coverage, which should report
    that there are 0 functions in this file.
    """
    return
