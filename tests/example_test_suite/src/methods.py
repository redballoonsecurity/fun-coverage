from abc import abstractmethod


class ExampleClass:
    """
    All methods below should be considered as functions and reported by fun-coverage.
    """

    def method(self):
        return

    async def async_method(self):
        return

    @staticmethod
    def static_method():
        """
        This function is not excluded from coverage.
        """
        return

    @abstractmethod
    def abstract_method(self):
        """
        Abstract methods should not be included in coverage reports.
        """
        raise NotImplementedError()
