def non_covered_function():
    """The tests shouldn't cover this function."""
    return


def non_covered_function_no_docstring(num_1, file_2):
    if num_1 > 2:
        a = 5 + num_1
    elif file_2:
        b = f"name_{file_2}"
    return a, b


def non_covered_function_no_docstring_multiline(
    num_1: int, file_2: str
) -> None:
    if num_1 > 2:
        a = 5 + num_1
    elif file_2:
        b = f"name_{file_2}"
    return a, b


def non_covered_function_multiline_no_body(
    arg_1, arg:2
) -> None:
    return


def non_covered_function_multiline_with_docstring_only(
  arg_1, arg_2
) -> None:
    """
    This docstring-only function should be marked as covered, as it is a docstring-only function
    """
