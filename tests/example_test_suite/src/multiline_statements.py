def normal_multiline_statement():
    return {
       "a": 100
    }


def odd_format_multiline_statement():
   return (f"foo"
      "bar")


def starts_with_assignment():
    args = [
        "a",
        "b",
        "c",
    ]
    return ";".join(args)
