def docstring_only_function():
    """
    This function has a docstring as unique statement. It should be reported as covered by fun-coverage.
    """
