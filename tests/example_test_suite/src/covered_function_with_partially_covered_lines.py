def covered_function_with_partially_covered_lines(x):
    if x == 1:
        # This statement will not be covered by the tests. But the function should be considered as covered anyway.
        return 1
    return 0
