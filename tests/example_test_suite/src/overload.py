from typing import overload


@overload
def return_something(value: None) -> str:
    ...


@overload
def return_something(value: bytes) -> bytes:
    ...


def return_something(value):
    if value is None:
        return "None"
    else:
        return value
