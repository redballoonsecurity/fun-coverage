# Changelog

`fun-coverage` adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), and this file is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [0.2.0]
- fun-coverage excludes functions with following decorators from function coverage reporting: abstractmethod, abstractclassmethod, abstractstaticmethod, abstractproperty, overload.

## [0.1.4]
- Fix bug in `get_funcs` which resulted in exclusion of all decorated functions from coverage analysis in Python 3.7 (!7).

## [0.1.3]
### Fixed
- Fix bug which resulted in functions with multi-line declarations always getting marked as covered when the module they are in is imported

## [0.1.2]
### Fixed
- Check to see if any lines in a function have coverage, as opposed to just the first line of a function. This adds better support for situations where coverage is not reported (for example, some multiline statements/list comprehensions)

## [0.1.1]
### Fixed
- Sort line numbers of the missed functions in the text report.

## [0.1.0]
Initial release.
